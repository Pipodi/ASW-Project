import {Component, OnInit} from "@angular/core";
import {IndexService} from "./index.service";
import {User} from "../model/user";
import {Router} from "@angular/router";
import {IMyDateRangeModel, IMyDrpOptions, IMyInputFieldChanged} from "mydaterangepicker";

@Component({
  selector: 'app-index',
  templateUrl: ('./index.component.html'),
  styleUrls: ['./index.component.css'],
  providers: [ IndexService ]
})

export class IndexComponent implements OnInit {

  private currentDate: Date;
  private checkInDateMillis: number = null;
  private checkOutDateMillis: number = null;

  private myDateRangePickerOptions: IMyDrpOptions;

  private user: User;

  private type: string;
  private cities: string[];
  private selectedCity: string = '';


  constructor(private service: IndexService, private router: Router) {}

  ngOnInit() {
    if(localStorage.getItem('email') != null) {
      this.user = new User(localStorage.getItem('email'), localStorage.getItem('name'), localStorage.getItem('surname'));
    } else {
      this.user = null;
    }
    this.type = 'friends';
    this.currentDate = new Date();
    this.myDateRangePickerOptions = {
      dateFormat: 'dd/mm/yyyy',
      disableUntil: {day: this.currentDate.getDate() -1 , month: this.currentDate.getMonth() + 1, year: this.currentDate.getFullYear()},
      dayLabels: {su: 'Dom', mo: 'Lun', tu: 'Mar', we: 'Mer', th: 'Gio', fr: 'Ven', sa: 'Sab'},
      monthLabels: { 1: 'Gen', 2: 'Feb', 3: 'Mar', 4: 'Apr', 5: 'Mag', 6: 'Giu', 7: 'Lug', 8: 'Ago', 9: 'Set', 10: 'Ott', 11: 'Nov', 12: 'Dic' },
      showSelectDateText: true,
      selectBeginDateTxt: 'Seleziona data check-in',
      selectEndDateTxt: 'Seleziona data check-out'
    };

    this.service.retrieveCities().then(result => this.cities = result.map(city => city.cityName));
  }

  onChangeType(type: string){
    this.type = type;
  }

  onRegister(): void{
    var email = document.getElementById('registerEmail') as HTMLInputElement;
    var password = document.getElementById('registerPassword') as HTMLInputElement;
    var name = document.getElementById('registerName') as HTMLInputElement;
    var surname = document.getElementById('registerSurname') as HTMLInputElement;

    var patternEmail = new RegExp('^\\w+@[a-zA-Z_]+?\\.[a-zA-Z]{2,3}$');

    if(email.value != '' && password.value != '' && name.value != '' && surname.value != '' && patternEmail.test(email.value)){
      this.service.register(email.value, password.value, name.value, surname.value).then(result => this.onRegisterResult(result));
    } else {
      var alertError = document.getElementById('alert-error') as HTMLElement;
      var alertSuccess = document.getElementById('alert-success') as HTMLElement;
      var message = document.getElementById('error-message') as HTMLElement;
      message.innerText = 'Riempire tutti i campi correttamente';
      alertError.style.display = 'block';
      alertSuccess.style.display = 'none';
    }
  }

  onLogin(): void {
    var email = document.getElementById('loginEmail') as HTMLInputElement;
    var password = document.getElementById('loginPassword') as HTMLInputElement;
    if(email.value != '' && password.value != ''){
      this.service.login(email.value, password.value).then(result => this.onLoginResult(result));
    } else {
      var alertError = document.getElementById('alert-error-login') as HTMLElement;
      var message = document.getElementById('error-message-login') as HTMLElement;
      message.innerText = 'Tutti i campi sono obbligatori';
      alertError.style.display = 'block';
    }
  }

  onSearch(): void {
    if (this.selectedCity != '' && this.checkInDateMillis != null && this.checkOutDateMillis != null) {
      this.router.navigate(['/home', this.selectedCity, this.checkInDateMillis.toString(), this.checkOutDateMillis.toString(), this.type]);
    } else {
      var alertError = document.getElementById('alert-error-search') as HTMLElement;
      alertError.style.display = 'block';
    }
  }

  onRegisterResult(result: string): void {
    console.log(result);
    if(result['success']){
      var alertSuccess = document.getElementById('alert-success') as HTMLElement;
      var alertError = document.getElementById('alert-error') as HTMLElement;
      alertSuccess.style.display = 'block';
      alertError.style.display = 'none';
    } else {
      var alertError = document.getElementById('alert-error') as HTMLElement;
      var alertSuccess = document.getElementById('alert-success') as HTMLElement;
      var message = document.getElementById('error-message') as HTMLElement;
      message.innerText = 'E-mail già in uso da un altro utente';
      alertError.style.display = 'block';
      alertSuccess.style.display = 'none';
    }
  }

  onLoginResult(result: string): void {
    console.log(result);
    if(result['success']) {
      this.user = result['user'] as User;
      localStorage.setItem("email", this.user.email);
      localStorage.setItem("name", this.user.userName);
      localStorage.setItem("surname", this.user.surname);
      var closeButton = document.getElementById('close-button-login');
      closeButton.click();
    } else {
      var alertErrorLogin = document.getElementById('alert-error-login') as HTMLElement;
      var alertMessageLogin = document.getElementById('error-message-login') as HTMLElement;
      alertErrorLogin.style.display = 'block';
      alertMessageLogin.innerText = 'E-Mail o Password errati';
    }
  }

  clearModals(){
    var alertError = document.getElementById('alert-error') as HTMLElement;
    var alertSuccess = document.getElementById('alert-success') as HTMLElement;
    var alertErrorLogin = document.getElementById('alert-error-login') as HTMLElement;
    alertSuccess.style.display = 'none';
    alertError.style.display = 'none';
    alertErrorLogin.style.display = 'none';

    // Resetto la form di login
    var email = document.getElementById('loginEmail') as HTMLInputElement;
    var password = document.getElementById('loginPassword') as HTMLInputElement;
    email.value = '';
    password.value = '';

    // Resetto la form di registrazione
    var email = document.getElementById('registerEmail') as HTMLInputElement;
    var password = document.getElementById('registerPassword') as HTMLInputElement;
    var name = document.getElementById('registerName') as HTMLInputElement;
    var surname = document.getElementById('registerSurname') as HTMLInputElement;
    email.value = '';
    password.value = '';
    name.value = '';
    surname.value = '';
  }

  onLogout():void{
    console.log("Logout");
    localStorage.removeItem("email");
    localStorage.removeItem("name");
    localStorage.removeItem("surname");
    this.user = null;
    this.router.navigate(['/index']);
  }

  onDateRangeChanged(event: IMyDateRangeModel) {
    this.checkInDateMillis = event.beginEpoc;
    this.checkOutDateMillis = event.endEpoc;
  }

  onInputFieldChanged(event: IMyInputFieldChanged) {
    if (!event.valid) {
      this.checkOutDateMillis = null;
      this.checkInDateMillis = null;
    }
  }

}
