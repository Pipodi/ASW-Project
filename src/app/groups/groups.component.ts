import {Component, OnInit} from "@angular/core";
import {GroupsService} from "./groups.service";
import {Group} from "../model/group";
import {User} from "../model/user";
import {Member} from "../model/member";
import {Itinerary} from "../model/itinerary";
import {PlaceItinerary} from "../model/place-itinerary";
import {Router} from "@angular/router";

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css'],
  providers: [ GroupsService ]
})
export class GroupsComponent implements OnInit {

  private user: User;
  private groupList: Group[] = [];
  private memberList: Member[] = [];

  private itineraries: Itinerary[] = [];

  private groupName: string;
  private currentGroup: Group;

  constructor(private service: GroupsService, private router: Router) {}

  ngOnInit() {
    if(localStorage.getItem('email') != null) {
      this.user = new User(localStorage.getItem('email'), localStorage.getItem('name'), localStorage.getItem('surname'));
    } else {
      this.user = null;
    }
    if(localStorage.getItem('email') != null){
      this.service.retrieveGroups(localStorage.getItem('email')).then(result => this.onRetrieveGroupsResult(result));
    }
  }

  onRetrieveGroupsResult(groups: Group[]): void {
    this.groupList = groups;

    if(groups != null && groups.length != 0){
      this.onMemberRequest(groups[0]);
      this.loadItineraries(groups[0].id);
      this.currentGroup = groups[0];
    }
  }

  onCreation():void{
    var groupNameInput = document.getElementById('group-name-input') as HTMLInputElement;
    var errorMessage = document.getElementById('error-message-group') as HTMLElement;
    var alertGroup = document.getElementById('alert-error-group') as HTMLElement;
    if (groupNameInput.value != ''){
      this.groupName = groupNameInput.value;
      alertGroup.style.display = "none";
      this.service.createGroup(groupNameInput.value, localStorage.getItem('email')).then(result => this.onCreationResult(result));
    }else {
      errorMessage.innerText = "Tutti i campi sono obbligatori.";
      alertGroup.style.display= "block";
    }
  }

  onCreationResult(result: string): void{
    if(result['success']){
      var alertSuccess = document.getElementById('alert-success-group') as HTMLElement;
      var alertError = document.getElementById('alert-error-group') as HTMLElement;
      alertSuccess.style.display = 'block';
      alertError.style.display = 'none';
      var newGroup = new Group(result['groupID'], this.groupName, localStorage.getItem('email'), 1);
      this.groupList.push(newGroup);
      console.log(newGroup.id);
      this.service.retrieveMembers(newGroup.id).then(result => this.memberList = result);
    } else {
      var alertError = document.getElementById('alert-error-group') as HTMLElement;
      var alertSuccess = document.getElementById('alert-success-group') as HTMLElement;
      var message = document.getElementById('error-message-group') as HTMLElement;
      message.innerText = 'Si è verificato un problema durante la creazione del gruppo.';
      alertError.style.display = 'block';
      alertSuccess.style.display = 'none';
    }
  }

  resetModals(){
    var groupNameInput = document.getElementById('group-name-input') as HTMLInputElement;
    var alertError = document.getElementById('alert-error-group') as HTMLElement;
    var alertSuccess = document.getElementById('alert-success-group') as HTMLElement;
    groupNameInput.value = '';
    alertError.style.display = 'none';
    alertSuccess.style.display = 'none';
  }

  onMemberRequest(group: Group): void {
    if(group != null){
      this.service.retrieveMembers(group.id).then(result => this.memberList = result);
      this.currentGroup = group;
      this.loadItineraries(group.id);
    }
  }

  isAdmin(email: string): boolean {
    if(this.currentGroup == null){
      return false;
    }
    return this.currentGroup.admin == email;
  }

  loadItineraries(groupID: string): void {
    this.service.loadItineraries(groupID).then(result => this.onResultItineraries(result));
  }

  onResultItineraries(result: string): void {
    this.itineraries = [];
    var currentItineraryID = '';
    var newItinerary = null;

    var places: PlaceItinerary[] = [];

    for(var i=0; i<result.length; i++){

      if(currentItineraryID != result[i]['id']){

        if(newItinerary != null){
          newItinerary.setPlaces(places);
          this.itineraries.push(newItinerary);
        }
        places = [];
        currentItineraryID = result[i]['id'];
        newItinerary = new Itinerary(result[i]['id'], result[i]['startDay'], result[i]['endDay'], result[i]['userName'],
          result[i]['surname'], result[i]['cityName']);
        places.push(new PlaceItinerary(result[i]['placeID'], result[i]['namePlace'], result[i]['dayRoute'], result[i]['image'],
                         result[i]['address'], result[i]['longDescription'], result[i]['url'], result[i]['linkType']));
        //
      } else {
        places.push(new PlaceItinerary(result[i]['placeID'], result[i]['namePlace'], result[i]['dayRoute'], result[i]['image'],
          result[i]['address'], result[i]['longDescription'], result[i]['url'], result[i]['linkType']));
      }
    }
    if(newItinerary != null){
      newItinerary.setPlaces(places);
      this.itineraries.push(newItinerary);
    }
  }

  onAddMember(): void {
    var alertSuccess = document.getElementById('alert-success-member') as HTMLElement;
    var alertError = document.getElementById('alert-error-member') as HTMLElement;
    var nameSelectedGroup = document.getElementById('name-selected-group') as HTMLElement;

    alertSuccess.style.display = 'none';
    alertError.style.display = 'none';

    nameSelectedGroup.innerText = 'nel gruppo ' + this.currentGroup.groupName;
  }

  onAddMemberConfirm(): void {
    var inputEmail = document.getElementById('member-email') as HTMLInputElement;
    var alertError = document.getElementById('alert-error-member') as HTMLElement;
    var errorMessage = document.getElementById('error-message-member') as HTMLElement;

    if(inputEmail.value == ''){
      errorMessage.innerText = 'Riempire il campo obbligatorio';
      alertError.style.display = 'block';
    } else {
      this.service.addMemberToGroup(inputEmail.value, this.currentGroup.id).then(result => this.onAddMemberResult(result));
    }
  }

  onAddMemberResult(result: string): void {
    var alertSuccess = document.getElementById('alert-success-member') as HTMLElement;
    var alertError = document.getElementById('alert-error-member') as HTMLElement;
    var errorMessage = document.getElementById('error-message-member') as HTMLElement;
    if(result['success']){
      alertError.style.display = 'none';
      alertSuccess.style.display = 'block';
      this.service.retrieveMembers(this.currentGroup.id).then(result => this.memberList = result);
    } else {
      errorMessage.innerText = 'Impossibile aggiungere il nuovo membro';
      alertError.style.display = 'block';
      alertSuccess.style.display = 'none';
    }
  }

  onLogout(): void {
    localStorage.removeItem("email");
    localStorage.removeItem("name");
    localStorage.removeItem("surname");
    this.user = null;
    this.router.navigate(['/index']);
  }

  deleteMemberFromGroup(userID: string, index: number): void {
    this.service.deleteMemberFromGroup(this.currentGroup.id, userID).then(result => this.onDeleteMemberResult(result, index));
  }

  onDeleteMemberResult(result: string, index: number): void {
    if(result['success']){
      this.memberList.splice(index, 1);
    } else {
      console.log('aiaaiaia');
    }
  }

  canDeleteMember(email: string): boolean {
    return this.isAdmin(this.user.email) && this.user.email != email;
  }

  exitFromGroup(group: Group, index: number){
    if(group.admin == this.user.email){
      // Elimino il gruppo
      this.service.deleteGroup(group.id).then(result => this.onDeletedGroup(result, index));
    } else {
      // Esco dal gruppo
      this.service.exitFromGroup(group.id, this.user.email).then(result => this.onDeletedGroup(result, index));
    }
  }

  onDeletedGroup(result: string, index: number): void {
    // Aggiorno inerfaccia passando ad un altro gruppo (se presente)
    // Altrimenti scrivo di creare un gruppo o di unirsi
    if(result['success']){
      this.groupList.splice(index, 1);
      if(this.groupList.length != 0){
        this.onMemberRequest(this.groupList[0]);
      } else {
        this.memberList = [];
        this.itineraries = [];
        this.currentGroup = null;
      }
    } else {
      // TODO errore
    }
  }

  openDetails(placeName: string, description: string, image: string, link: string, linkType: string, address: string): void {
    var button = document.getElementById('button-details-modal') as HTMLElement;
    button.click();

    var detailsPlaceName = document.getElementById('detail-place-name') as HTMLElement;
    var detailPlaceImage = document.getElementById('detail-place-image') as HTMLElement;
    var detailPlaceLink = document.getElementById('detail-place-link') as HTMLElement;
    var detailPlaceDescription = document.getElementById('detail-place-description') as HTMLElement;
    var detailPlaceAddress = document.getElementById('detail-place-address') as HTMLElement;

    detailsPlaceName.innerText = placeName;

    detailPlaceLink.setAttribute('href', link);
    detailPlaceLink.innerText = linkType;

    detailPlaceImage.setAttribute('src', image);

    detailPlaceDescription.innerText = description;

    detailPlaceAddress.innerText = address;
  }

}
