import { Injectable } from '@angular/core';
import {Http} from "@angular/http";
import {Group} from "../model/group";
import {Member} from "../model/member";

@Injectable()
export class GroupsService {

  private urlCreation: string = 'http://ubook.store/svapo/createGroup.php';
  private urlGroups: string = 'http://ubook.store/svapo/groups.php';
  private urlMember: string = 'http://ubook.store/svapo/members.php';
  private urlItineraries: string = 'http://ubook.store/svapo/itineraries.php';
  private urlAddMember: string = 'http://ubook.store/svapo/addMember.php';
  private urlDeleteMember: string = 'http://ubook.store/svapo/deleteMember.php';
  private urlDeleteGroup: string = 'http://ubook.store/svapo/deleteGroup.php';
  private urlExitFromGroup: string = 'http://ubook.store/svapo/exitFromGroup';

  constructor(private http: Http) { }

  createGroup(groupName: string, userID: string): Promise<string>{
    var headers = new Headers();
    headers.append('Access-Control-Allow-Origin','*');
    headers.append('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers');
    headers.append('Content-Type', 'application/json');

    var params = {name: groupName, userID: userID};

    return this.http.post(this.urlCreation, JSON.stringify(params), headers).toPromise()
      .then(result => result.json());
  }

  retrieveGroups(userID: string): Promise<Group[]>{
    var headers = new Headers();
    headers.append('Access-Control-Allow-Origin','*');
    headers.append('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers');
    headers.append('Content-Type', 'application/json');

    var params = {email: userID};

    return this.http.post(this.urlGroups, JSON.stringify(params), headers).toPromise()
      .then(result => result.json() as Group[]);
  }

  retrieveMembers(groupID: string): Promise<Member[]> {
    var headers = new Headers();
    headers.append('Access-Control-Allow-Origin','*');
    headers.append('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers');
    headers.append('Content-Type', 'application/json');

    var params = {groupID: groupID};

    return this.http.post(this.urlMember, JSON.stringify(params), headers).toPromise()
      .then(result => result.json() as Member[]);
  }

  loadItineraries(groupID: string): Promise<string>{
    var headers = new Headers();
    headers.append('Access-Control-Allow-Origin','*');
    headers.append('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers');
    headers.append('Content-Type', 'application/json');

    var params = {groupID: groupID};

    return this.http.post(this.urlItineraries, JSON.stringify(params), headers).toPromise()
      .then(result => result.json());
  }

  addMemberToGroup(email: string, groupID: string): Promise<string> {
    var headers = new Headers();
    headers.append('Access-Control-Allow-Origin','*');
    headers.append('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers');
    headers.append('Content-Type', 'application/json');

    var params = {email: email, groupID: groupID};

    return this.http.post(this.urlAddMember, JSON.stringify(params), headers).toPromise()
      .then(result => result.json());
  }

  deleteMemberFromGroup(groupID: string, userID: string): Promise<string> {
    var headers = new Headers();
    headers.append('Access-Control-Allow-Origin','*');
    headers.append('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers');
    headers.append('Content-Type', 'application/json');

    var params = {email: userID, groupID: groupID};

    return this.http.post(this.urlDeleteMember, JSON.stringify(params), headers).toPromise()
      .then(result => result.json());
  }

  deleteGroup(groupID: string): Promise<string> {
    var headers = new Headers();
    headers.append('Access-Control-Allow-Origin','*');
    headers.append('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers');
    headers.append('Content-Type', 'application/json');

    var params = {groupID: groupID};

    return this.http.post(this.urlDeleteGroup, JSON.stringify(params), headers).toPromise()
      .then(result => result.json());
  }

  exitFromGroup(groupID: string, userID: string): Promise<string> {
    var headers = new Headers();
    headers.append('Access-Control-Allow-Origin','*');
    headers.append('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers');
    headers.append('Content-Type', 'application/json');

    var params = {email: userID, groupID: groupID};

    return this.http.post(this.urlExitFromGroup, JSON.stringify(params), headers).toPromise()
      .then(result => result.json());
  }

}
