import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {City} from "../model/city";

@Injectable()
export class SuggestService {

  private urlCities: string = 'http://ubook.store/svapo/cities.php';
  private urlRegister: string = 'http://ubook.store/svapo/registerPost.php';
  private urlLogin: string = 'http://ubook.store/svapo/login.php';
  private urlSuggest: string = 'http://ubook.store/svapo/suggest.php';

  constructor(private http: Http) {}

  retrieveCities(): Promise<City[]> {
    var headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers');
    headers.append('Content-Type', 'application/json');

    var params = {};

    return this.http.post(this.urlCities, JSON.stringify(params), headers)
      .toPromise().then(result => result.json() as City[]);
  }

  register(email: string, password: string, name: string, surname: string): Promise<string> {
    var headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers');
    headers.append('Content-Type', 'application/json');

    var params = {email: email, password: password, name: name, surname: surname};

    return this.http.post(this.urlRegister, JSON.stringify(params), headers)
      .toPromise().then(result => result.json());
  }

  login(email: string, password: string): Promise<string> {
    var headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers');
    headers.append('Content-Type', 'application/json');

    var params = {email: email, password: password};

    return this.http.post(this.urlLogin, JSON.stringify(params), headers)
      .toPromise().then(result => result.json());
  }

  suggest(city: string, place: string, description: string, image: string, lat: number, lng: number, duration: number): Promise<string> {
    var headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers');
    headers.append('Content-Type', 'application/json');

    var params = {city: city, place: place, description: description, image: image, lat: lat, lng: lng, duration: duration};

    return this.http.post(this.urlSuggest, JSON.stringify(params), headers)
      .toPromise().then(result => result.json());
  }


}
