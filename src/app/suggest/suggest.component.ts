import {Component, OnInit} from "@angular/core";
import {User} from "../model/user";
import {SuggestService} from "./suggest.service";
import {Router} from "@angular/router";
import {IMyDateRangeModel, IMyDrpOptions, IMyInputFieldChanged} from "mydaterangepicker";

@Component({
  selector: 'app-suggest',
  templateUrl: './suggest.component.html',
  styleUrls: ['./suggest.component.css'],
  providers: [SuggestService]
})
export class SuggestComponent implements OnInit {

  private currentDate: Date;
  private checkInDateMillis: number = null;
  private checkOutDateMillis: number = null;

  private myDateRangePickerOptions: IMyDrpOptions;

  private user: User;

  private type: string;
  private cities: string[];
  private selectedCity: string = '';
  private selectedSuggestedCity: string = '';

  constructor(private service: SuggestService, private router: Router) {
  }

  ngOnInit() {
    if (localStorage.getItem('email') != null) {
      this.user = new User(localStorage.getItem('email'), localStorage.getItem('name'), localStorage.getItem('surname'));
    } else {
      this.user = null;
    }
    this.type = 'friends';
    this.currentDate = new Date();
    this.myDateRangePickerOptions = {
      dateFormat: 'dd/mm/yyyy',
      disableUntil: {
        day: this.currentDate.getDate() - 1,
        month: this.currentDate.getMonth() + 1,
        year: this.currentDate.getFullYear()
      },
      dayLabels: {su: 'Dom', mo: 'Lun', tu: 'Mar', we: 'Mer', th: 'Gio', fr: 'Ven', sa: 'Sab'},
      monthLabels: {
        1: 'Gen',
        2: 'Feb',
        3: 'Mar',
        4: 'Apr',
        5: 'Mag',
        6: 'Giu',
        7: 'Lug',
        8: 'Ago',
        9: 'Set',
        10: 'Ott',
        11: 'Nov',
        12: 'Dic'
      },
      showSelectDateText: true,
      selectBeginDateTxt: 'Seleziona data check-in',
      selectEndDateTxt: 'Seleziona data check-out'
    };

    this.service.retrieveCities().then(result => this.cities = result.map(city => city.cityName));
  }

  onChangeType(type: string) {
    this.type = type;
  }

  onRegister(): void {
    var email = document.getElementById('registerEmail') as HTMLInputElement;
    var password = document.getElementById('registerPassword') as HTMLInputElement;
    var name = document.getElementById('registerName') as HTMLInputElement;
    var surname = document.getElementById('registerSurname') as HTMLInputElement;

    if (email.value != '' && password.value != '' && name.value != '' && surname.value != '') {
      this.service.register(email.value, password.value, name.value, surname.value).then(result => this.onRegisterResult(result));
    } else {
      var alertError = document.getElementById('alert-error') as HTMLElement;
      var alertSuccess = document.getElementById('alert-success') as HTMLElement;
      var message = document.getElementById('error-message') as HTMLElement;
      message.innerText = 'Tutti i campi sono obbligatori';
      alertError.style.display = 'block';
      alertSuccess.style.display = 'none';
    }
  }

  onLogin(): void {
    var email = document.getElementById('loginEmail') as HTMLInputElement;
    var password = document.getElementById('loginPassword') as HTMLInputElement;
    if (email.value != '' && password.value != '') {
      this.service.login(email.value, password.value).then(result => this.onLoginResult(result));
    } else {
      var alertError = document.getElementById('alert-error-login') as HTMLElement;
      var message = document.getElementById('error-message-login') as HTMLElement;
      message.innerText = 'Tutti i campi sono obbligatori';
      alertError.style.display = 'block';
    }
  }

  search(): void {
    if (this.selectedCity != '' && this.checkInDateMillis != null && this.checkOutDateMillis != null) {
      this.router.navigate(['/home', this.selectedCity, this.checkInDateMillis.toString(), this.checkOutDateMillis.toString(), this.type]);
    } else {
      var alertError = document.getElementById('alert-error-search') as HTMLElement;
      alertError.style.display = 'block';
    }
  }

  onRegisterResult(result: string): void {
    console.log(result);
    if (result['success']) {
      var alertSuccess = document.getElementById('alert-success') as HTMLElement;
      var alertError = document.getElementById('alert-error') as HTMLElement;
      alertSuccess.style.display = 'block';
      alertError.style.display = 'none';
    } else {
      var alertError = document.getElementById('alert-error') as HTMLElement;
      var alertSuccess = document.getElementById('alert-success') as HTMLElement;
      var message = document.getElementById('error-message') as HTMLElement;
      message.innerText = 'E-mail già in uso da un altro utente';
      alertError.style.display = 'block';
      alertSuccess.style.display = 'none';
    }
  }

  onLoginResult(result: string): void {
    console.log(result);
    if (result['success']) {
      this.user = result['user'] as User;
      localStorage.setItem("email", this.user.email);
      localStorage.setItem("name", this.user.userName);
      localStorage.setItem("surname", this.user.surname);
      var closeButton = document.getElementById('close-button-login');
      closeButton.click();
    } else {
      var alertErrorLogin = document.getElementById('alert-error-login') as HTMLElement;
      var alertMessageLogin = document.getElementById('error-message-login') as HTMLElement;
      alertErrorLogin.style.display = 'block';
      alertMessageLogin.innerText = 'E-Mail o Password errati';
    }
  }

  clearModals() {
    var alertError = document.getElementById('alert-error') as HTMLElement;
    var alertSuccess = document.getElementById('alert-success') as HTMLElement;
    var alertErrorLogin = document.getElementById('alert-error-login') as HTMLElement;
    alertSuccess.style.display = 'none';
    alertError.style.display = 'none';
    alertErrorLogin.style.display = 'none';

    // Resetto la form di login
    var email = document.getElementById('loginEmail') as HTMLInputElement;
    var password = document.getElementById('loginPassword') as HTMLInputElement;
    email.value = '';
    password.value = '';

    // Resetto la form di registrazione
    var email = document.getElementById('registerEmail') as HTMLInputElement;
    var password = document.getElementById('registerPassword') as HTMLInputElement;
    var name = document.getElementById('registerName') as HTMLInputElement;
    var surname = document.getElementById('registerSurname') as HTMLInputElement;
    email.value = '';
    password.value = '';
    name.value = '';
    surname.value = '';
  }

  onLogout(): void {
    console.log("Logout");
    localStorage.removeItem("email");
    localStorage.removeItem("name");
    localStorage.removeItem("surname");
    this.user = null;
    this.router.navigate(['/index']);
  }

  onDateRangeChanged(event: IMyDateRangeModel) {
    this.checkInDateMillis = event.beginEpoc;
    this.checkOutDateMillis = event.endEpoc;
  }

  onInputFieldChanged(event: IMyInputFieldChanged) {
    if (!event.valid) {
      this.checkOutDateMillis = null;
      this.checkInDateMillis = null;
    }
  }

  onSuggest(): void {
    var city = document.getElementById('suggest-place-city') as HTMLInputElement;
    var name = document.getElementById('suggest-place-name') as HTMLInputElement;
    var description = document.getElementById('suggest-place-description') as HTMLInputElement;
    var image = document.getElementById('suggest-place-image') as HTMLInputElement;
    var lat = document.getElementById('suggest-place-lat') as HTMLInputElement;
    var long = document.getElementById('suggest-place-long') as HTMLInputElement;
    var duration = document.getElementById('suggest-place-duration') as HTMLInputElement;

    var patternCoordinate = new RegExp('^-?[0-9]{1,2}\.[0-9]{6}$');
    var patternInteger = new RegExp('^[0-9]+$');

    if(city.value == '' || name.value == '' || description.value == '' || image.value == '' || lat.value == ''
      || long.value == '' || duration.value == '' || !patternCoordinate.test(lat.value) || !patternCoordinate.test(long.value)
      || !patternInteger.test(duration.value)){
      var buttonDialog = document.getElementById('button-modal-suggest') as HTMLElement;
      var message = document.getElementById('suggest-dialog-message') as HTMLElement;
      message.innerText = 'Non è stato possibile inviare il tuo suggerimento! Riempi tutti i campi correttamente!';
      buttonDialog.click();
    } else {
      this.service.suggest(city.value, name.value, description.value, image.value, parseFloat(lat.value),
        parseFloat(long.value), parseInt(duration.value)).then(result => this.onSuggestResult(result));
    }
  }

  onSuggestResult(result: string): void {
    var dialog = document.getElementById('suggest-dialog') as HTMLElement;
    var buttonDialog = document.getElementById('button-modal-suggest') as HTMLElement;
    var message = document.getElementById('suggest-dialog-message') as HTMLElement;
    if(result['success']){
      message.innerText = 'Grazie dell\'aiuto! Il tuo posto sarà revisionato al più presto.';

      var city = document.getElementById('suggest-place-city') as HTMLInputElement;
      var name = document.getElementById('suggest-place-name') as HTMLInputElement;
      var description = document.getElementById('suggest-place-description') as HTMLInputElement;
      var image = document.getElementById('suggest-place-image') as HTMLInputElement;
      var lat = document.getElementById('suggest-place-lat') as HTMLInputElement;
      var long = document.getElementById('suggest-place-long') as HTMLInputElement;
      var duration = document.getElementById('suggest-place-duration') as HTMLInputElement;

      city.value = '';
      name.value = '';
      description.value = '';
      image.value = '';
      image.value = '';
      lat.value = '';
      long.value = '';
      duration.value = '';
    } else {
      message.innerText = 'Non è stato possibile inviare il tuo suggerimento! Riprova più tardi.';
    }
    buttonDialog.click();
  }

}
