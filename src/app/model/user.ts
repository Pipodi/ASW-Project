export class User {

  email: string;
  userName: string;
  surname: string;

  constructor(email: string, userName: string, surname: string){
    this.email = email;
    this.userName = userName;
    this.surname = surname;
  }

}
